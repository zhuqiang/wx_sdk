package cn.mrcode.wxsdk.web.curator;

import cn.mrcode.wxsdk.core.context.ConfigContext;
import cn.mrcode.wxsdk.core.context.ParseConfig;
import cn.mrcode.wxsdk.core.zkhelper.curator.ClientHelper;
import cn.mrcode.wxsdk.core.zkhelper.curator.FindRuningLockPathHelper;
import cn.mrcode.wxsdk.web.common.ticket.lifeCycle.distributed.strategy.master.TicketDistributedTaskMaster;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 查找leader的节点路径
 * @author zhuqiang
 */
public class FindTicketRuningLockPath{
    private static Logger log = LoggerFactory.getLogger(FindTicketRuningLockPath.class);
    protected static int sessionTimeout;
    protected static String zkServiceList;

    public FindTicketRuningLockPath() {
        ParseConfig parseConfig = new ParseConfig();
        ConfigContext configContext = parseConfig.parse();
        configContext.setId(ConfigContext.getCurrentIPId() + ":manual update");
        ConfigContext.DistributedConfig dirstributedConfig = configContext.getDirstributedConfig();

        sessionTimeout = dirstributedConfig.getSessionTimeout();
        zkServiceList = dirstributedConfig.getZkServiceList();
    }

    @Test
    public void findLeader(){
        ArrayList<String> list = FindRuningLockPathHelper.find(ClientHelper.createClient(zkServiceList,sessionTimeout), TicketDistributedTaskMaster.masterPath);
        System.out.println("======== 匹配runing id 的路径有 =========");
        System.out.println(Arrays.toString(list.toArray()));
    }
    @Test
    public void findTager(){
        String targetChildPath = "0.0.0.0:not get ip";
        ArrayList<String> list = FindRuningLockPathHelper.find(ClientHelper.createClient(zkServiceList,sessionTimeout), TicketDistributedTaskMaster.masterPath,targetChildPath);
        System.out.println("======== 匹配 id 的路径有 =========");
        System.out.println(Arrays.toString(list.toArray()));
    }
}

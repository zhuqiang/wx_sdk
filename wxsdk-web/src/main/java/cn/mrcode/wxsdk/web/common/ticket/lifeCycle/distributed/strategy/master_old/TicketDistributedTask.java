package cn.mrcode.wxsdk.web.common.ticket.lifeCycle.distributed.strategy.master_old;

import cn.mrcode.wxsdk.core.dialogue.common.LogUtil;
import cn.mrcode.wxsdk.core.dialogue.common.distributed.ZkDistributedSingleNodeExecutor;
import cn.mrcode.wxsdk.core.dialogue.common.exception.ReqException;
import cn.mrcode.wxsdk.core.dialogue.common.exception.WxException;
import cn.mrcode.wxsdk.core.dialogue.common.log.LogTemplateUtil;
import cn.mrcode.wxsdk.core.dialogue.common.util.DateUtil;
import cn.mrcode.wxsdk.web.common.ticket.TicketApi;
import cn.mrcode.wxsdk.web.protocol.base.JsApiTicketInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.DelayQueue;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 分布式 ticket 维护task类
 * @date 2015/9/28 13:29
 */
@Deprecated
public class TicketDistributedTask implements Runnable {
    private static Logger log = LoggerFactory.getLogger(TicketDistributedTask.class);
    private ZkDistributedSingleNodeExecutor zsnf;
    private Map<String, JsApiTicketInfo> taskMap;  //用于任务中 更新的 ticket 列表.全部打成数组上传 zk上
    private DelayQueue<JsApiTicketInfo> queue; // 队列中的数据 和 taskMap一致。也需要事先准备好给task

    /**
     *
     * @param zsnf
     * @param taskMap map中的ticket不会动态增加了
     */
    public TicketDistributedTask(ZkDistributedSingleNodeExecutor zsnf, Map<String, JsApiTicketInfo> taskMap) {
        this.zsnf = zsnf;
        this.taskMap = taskMap;
        queue = new DelayQueue<JsApiTicketInfo>();
        for (Map.Entry<String,JsApiTicketInfo> ent : taskMap.entrySet()){
            queue.add(ent.getValue());
        }
    }

    @Override
    public void run() {
        String apiName = "分布式JsApiTicketTask";
        while (true) {
            try {
                queue.clear();
                for (Map.Entry<String,JsApiTicketInfo> ent : taskMap.entrySet()){
                    queue.add(ent.getValue());
                }
                log.info(LogTemplateUtil.svMsg(apiName,"taskMap.size=%s;queue.size=%s",taskMap.size()+"",queue.size()+""));
                JsApiTicketInfo ati = queue.take();  //获取超时的 对象
                log.info(LogTemplateUtil.svMsg(apiName,"%s,id=%s;获取到超时JsApiTicketInfo，数据=%s；超时时间=%s",
                        zsnf.getRootPath(),zsnf.getThisPath(),
                        JSONObject.toJSONString(ati), DateUtil.lFormat(ati.getTimeOut())));
                String appID = ati.getAppID();
                try {
                    ati = TicketApi.getJsApiTicketInfoSetUpBy(appID, ati.getAppSecret(),zsnf.getThisPath());
                } catch (ReqException e) {
                    log.error(LogTemplateUtil.svMsg(apiName,"%s,id=%s;appid=%s;" + LogUtil.fromateLog(e),zsnf.getRootPath(),zsnf.getThisPath(),appID));
                } catch (WxException e) {
                    log.error(LogTemplateUtil.svMsg(apiName,"%s,id=%s;appid=%s;" + LogUtil.fromateLog(e),zsnf.getRootPath(),zsnf.getThisPath(),appID));
                }
                taskMap.put(appID, ati);
                queue.add(ati);
                log.info(LogTemplateUtil.svMsg(apiName,"%s,id=%s;appID=%s 已经更新；新的JsApiTicketInfo数据=%s；超时时间=%s",
                        zsnf.getRootPath(),zsnf.getThisPath(),
                        appID, JSONObject.toJSONString(ati), DateUtil.lFormat(ati.getTimeOut())));
                //写入 zk上
                Collection<JsApiTicketInfo> values = taskMap.values();
                List<JsApiTicketInfo> data = new ArrayList<JsApiTicketInfo>(values);
                zsnf.writeData(JSON.toJSONString(data));
            } catch (InterruptedException e) {
                log.error(LogTemplateUtil.svMsg(apiName,"%s,id=%s;任务被中断，异常信息=%s",zsnf.getRootPath(),zsnf.getThisPath(), JSONObject.toJSONString(e)));
            }
        }
    }
}

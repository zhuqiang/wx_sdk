package cn.mrcode.wxsdk.web.protocol.req;

/**
 * 调用网页授权接口 的  access_token 与基础支持的 access_token 不同<br/>
 * 此 access_token 的含义是：针对每个openid的权限
 * 基础支持的acces_token 的含义是：针对每个公众号的权限
 * @author zhuqiang
 * @version V1.0
 * @date 2015/9/18 11:15
 */
public class WebAccessTokenReqData {
    private String appid;//appid	是	公众号的唯一标识
    private String secret;//secret	是	公众号的appsecret
    private String code;//code	是	填写第一步获取的code参数
    private String grant_type;//grant_type	是	填写为authorization_code

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }
}

package cn.mrcode.wxsdk.web.common.ticket.lifeCycle;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 获取 ticket接口
 * @date 2015/9/28 10:54
 */
public interface ITicketService {
    /**
     * 获取 jsSdk 授权的 ticket
     * @param appid
     * @param secret
     * @return
     */
    String getJsApiTicke(String appid, String secret) throws Exception ;
}

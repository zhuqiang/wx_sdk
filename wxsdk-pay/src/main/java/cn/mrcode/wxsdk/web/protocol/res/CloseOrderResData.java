package cn.mrcode.wxsdk.web.protocol.res;


import cn.mrcode.wxsdk.web.protocol.PayResData;

/**
 * 关闭订单回包数据
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/26 11:10
 */
public class CloseOrderResData implements PayResData {
    // 协议 返回结果
    private String return_code; //返回状态码	return_code	是	String(16)	SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
    private String return_msg; //返回信息	return_msg	否	String(128)	签名失败返回信息，如非空，为错误原因 ，签名失败 ，参数格式校验错误

    // 以下字段在return_code为SUCCESS的时候有返回值
    private String appid;  //    公众账号ID	appid	是	String(32)	wx8888888888888888	微信分配的公众账号ID（企业号corpid即为此appId）
    private String mch_id; //    商户号	mch_id	是	String(32)	1900000109	微信支付分配的商户号
    private String device_info;//    设备号	device_info	否	String(32)	013467007045764	调用接口提交的终端设备号，
    private String nonce_str;//    随机字符串	nonce_str	是	String(32)	5K8264ILTKCH16CQ2502SI8ZNMTM67VS	微信返回的随机字符串
    private String sign;//    签名	sign	是	String(32)	C380BEC2BFD727A4B6845133519F3AD6	微信返回的签名，详见签名算法
    private String err_code;//    错误代码		否	String(32)	SYSTEMERROR	详细参见第6节错误列表
    private String err_code_des; ////    错误代码描述		否	String(128)	系统错误	错误返回的信息描述

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getDevice_info() {
        return device_info;
    }

    public void setDevice_info(String device_info) {
        this.device_info = device_info;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getErr_code() {
        return err_code;
    }

    public void setErr_code(String err_code) {
        this.err_code = err_code;
    }

    public String getErr_code_des() {
        return err_code_des;
    }

    public void setErr_code_des(String err_code_des) {
        this.err_code_des = err_code_des;
    }
}

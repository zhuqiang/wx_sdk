package cn.mrcode.wxsdk.web.common;

/**
 * 支付错误码常量，用于业务判断对比操作,待重构？ 发现api接口里面的错误码有一部分是重合的。先写到一起把。以后看情况再分离
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/26 9:14
 */
public class PayErrCode {
    public static final String FAIL = "FAIL";  //result_code 业务返回结果。失败
    public static final String SUCCESS = "SUCCESS";  //result_code 业务返回结果。成功

    public static final String NOAUTH ="NOAUTH"; //NOAUTH	商户无此接口权限	商户未开通此接口权限	请商户前往申请此接口权限
    public static final String NOTENOUGH ="NOTENOUGH";//NOTENOUGH	余额不足	用户帐号余额不足	用户帐号余额不足，请用户充值或更换支付卡后再支付
    public static final String ORDERPAID ="ORDERPAID";//ORDERPAID	商户订单已支付	商户订单已支付，无需重复操作	商户订单已支付，无需更多操作
    public static final String ORDERCLOSED ="ORDERCLOSED";//ORDERCLOSED	订单已关闭	当前订单已关闭，无法支付	当前订单已关闭，请重新下单
    public static final String SYSTEMERROR ="SYSTEMERROR";//SYSTEMERROR	系统错误	系统超时	系统异常，请用相同参数重新调用
    public static final String APPID_NOT_EXIST ="APPID_NOT_EXIST";//APPID_NOT_EXIST	APPID不存在	参数中缺少APPID	请检查APPID是否正确
    public static final String MCHID_NOT_EXIST ="MCHID_NOT_EXIST";//MCHID_NOT_EXIST	MCHID不存在	参数中缺少MCHID	请检查MCHID是否正确
    public static final String APPID_MCHID_NOT_MATCH ="APPID_MCHID_NOT_MATCH";//APPID_MCHID_NOT_MATCH	appid和mch_id不匹配	appid和mch_id不匹配	请确认appid和mch_id是否匹配
    public static final String LACK_PARAMS ="LACK_PARAMS";//LACK_PARAMS	缺少参数	缺少必要的请求参数	请检查参数是否齐全
    public static final String OUT_TRADE_NO_USED ="OUT_TRADE_NO_USED";//OUT_TRADE_NO_USED	商户订单号重复	同一笔交易不能多次提交	请核实商户订单号是否重复提交
    public static final String SIGNERROR ="SIGNERROR";//SIGNERROR	签名错误	参数签名结果不正确	请检查签名参数和方法是否都符合签名算法要求
    public static final String XML_FORMAT_ERROR ="XML_FORMAT_ERROR";//XML_FORMAT_ERROR	XML格式错误	XML格式错误	请检查XML参数格式是否正确
    public static final String REQUIRE_POST_METHOD ="REQUIRE_POST_METHOD";//REQUIRE_POST_METHOD	请使用post方法	未使用post传递参数 	请检查请求参数是否通过post方法提交
    public static final String POST_DATA_EMPTY ="POST_DATA_EMPTY";//POST_DATA_EMPTY	post数据为空	post数据不能为空	请检查post数据是否为空
    public static final String NOT_UTF8 ="NOT_UTF8";//NOT_UTF8	编码格式错误	未使用指定编码格式	请使用NOT_UTF8编码格式

    // 关单特有的错误码，其他只是说明不一致
    public static final String  ORDERNOTEXIST = "ORDERNOTEXIST";//ORDERNOTEXIST	订单不存在	订单系统不存在此订单	不需要关单，当作未提交的支付的订单


    // 申请退款特有错误码，其他只是说明不一致
    public static final String INVALID_TRANSACTIONID = "INVALID_TRANSACTIONID" ;//INVALID_TRANSACTIONID	无效transaction_id	请求参数未按指引进行填写	请求参数错误，检查原交易号是否存在或发起支付交易接口返回失败
    public static final String PARAM_ERROR = "PARAM_ERROR"; //PARAM_ERROR	参数错误	请求参数未按指引进行填写	请求参数错误，请重新检查再调用退款申请

}

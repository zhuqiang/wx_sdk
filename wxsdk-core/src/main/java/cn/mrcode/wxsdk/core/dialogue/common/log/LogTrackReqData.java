package cn.mrcode.wxsdk.core.dialogue.common.log;

import java.util.Date;

/**
 * 日志追踪链请求包
 *
 * @author zhuqiang
 */
public class LogTrackReqData {
    //请求开始时间
    private Date startTime;
    //请求结束时间
    private Date endTime;
    //当前请求追踪id
    private String reqId;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }
}

package cn.mrcode.wxsdk.core.dialogue.common.config;


import cn.mrcode.wxsdk.core.dialogue.common.PublicAccount;

import java.util.HashMap;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 初始化参数信息类
 * @date 2015/9/25 10:28
 */
public class InitConfig {
    private boolean mode;  //是否支持分布式模式
    private HashMap<String, PublicAccount> payPublicAccountMap;//支付公众号信息
    private DistributedConfig dirstributedConfig; //分布式参数配置信息
    private String httpClientClassName; //自定义http请求器

    public boolean isMode() {
        return mode;
    }

    public void setMode(boolean mode) {
        this.mode = mode;
    }

    public HashMap<String, PublicAccount> getPayPublicAccountMap() {
        return payPublicAccountMap;
    }

    public void setPayPublicAccountMap(HashMap<String, PublicAccount> payPublicAccountMap) {
        this.payPublicAccountMap = payPublicAccountMap;
    }

    public DistributedConfig getDirstributedConfig() {
        return dirstributedConfig;
    }

    public void setDirstributedConfig(DistributedConfig dirstributedConfig) {
        this.dirstributedConfig = dirstributedConfig;
    }

    public String getHttpClientClassName() {
        return httpClientClassName;
    }

    public void setHttpClientClassName(String httpClientClassName) {
        this.httpClientClassName = httpClientClassName;
    }

    public static class DistributedConfig {
        private int sessionTimeout; //zookeeper链接超时时间，单位毫秒
        private String zkServiceList; //zookeeper 服务器列表，中间用 , 逗号隔开
        private HashMap<String, PublicAccount> publicAccountMap; //需要维护的公众号 列表，在使用的时候，所用公众号必须在此列表中

        public int getSessionTimeout() {
            return sessionTimeout;
        }

        public void setSessionTimeout(int sessionTimeout) {
            this.sessionTimeout = sessionTimeout;
        }

        public String getZkServiceList() {
            return zkServiceList;
        }

        public void setZkServiceList(String zkServiceList) {
            this.zkServiceList = zkServiceList;
        }

        public HashMap<String, PublicAccount> getPublicAccountMap() {
            return publicAccountMap;
        }

        public void setPublicAccountMap(HashMap<String, PublicAccount> publicAccountMap) {
            this.publicAccountMap = publicAccountMap;
        }
    }
}


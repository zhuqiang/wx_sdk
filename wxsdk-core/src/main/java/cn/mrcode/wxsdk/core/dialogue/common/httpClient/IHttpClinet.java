package cn.mrcode.wxsdk.core.dialogue.common.httpClient;


import cn.mrcode.wxsdk.core.dialogue.common.exception.ReqException;

/**
 * @author zhuqiang
 * @version V1.0
 * @date 2015/11/2 11:08
 * 请求器接口
 */
public interface IHttpClinet {
    /**
     * get请求
     * @param url
     * @return
     */
    String get(String url)throws ReqException;

    /**
     * post json请求
     * @param url
     * @param body json串
     * @return
     * @throws ReqException
     */
    String postJson(String url, String body) throws ReqException;

    /**
     * post xml请求
     * @param url
     * @param xml
     * @param appid 如果需要使用证书访问，就必须传递该值
     * @return
     * @throws ReqException
     */
    String postXml(String url, String xml, String appid)throws ReqException;
}

package cn.mrcode.wxsdk.core.dialogue.common.config;

/**
 * @author zhuqiang
 * @version V1.0
 * @date 2015/11/17 13:53
 */
public interface InitConstans {
    /** 支付公众号节点 */
    String node_payPublicAccountList = "payPublicAccountList";
    /** 账户节点 **/
    String node_account = "account";
    /** 属性节点 **/
    String node_property = "property";


    /** 是否是分布式节点标志 */
    String node_mode  = "mode";
    /**  **/
    String node_httpClientClassName = "httpClientClassName";

    /** 分布式参数 */
    String distributedConfig = "distributedConfig";
    String xPath_sessionTimeout = "property[@name='sessionTimeout']";
    String xPath_zkServiceList = "property[@name='zkServiceList']";
    String xPath_baseTokenRootPath = "property[@name='baseTokenRootPath']";
    String xPath_jssdkTicketRootPath = "property[@name='jssdkTicketRootPath']";
    String xPath_publicAccountList ="property[@name='publicAccountList']";

}

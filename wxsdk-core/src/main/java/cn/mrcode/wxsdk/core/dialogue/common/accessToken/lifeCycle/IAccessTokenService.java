package cn.mrcode.wxsdk.core.dialogue.common.accessToken.lifeCycle;

/**
 * 接口
 * @author zhuqiang
 * @version V1.0
 * @date 2015/9/22 17:23
 */
public interface IAccessTokenService {
    /**
     * 本方法支持 多公众号获取Accesstoken信息，并且维护Accesstoken的生存时间;<br>
     *     目前分为单机版和分布式实现类。请对应场景给定实例
     * @param appid
     * @param secret
     * @return
     */
    public String getAccesstoken(String appid, String secret) throws Exception;
}

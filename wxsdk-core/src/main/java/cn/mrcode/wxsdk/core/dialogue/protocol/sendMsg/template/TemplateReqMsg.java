package cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template;


import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.template.Template;

/**
 * 发送模版消息 数据请求包
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/10 11:00
 */
public class TemplateReqMsg {
    private String touser;  //openid
    private String template_id; //模版消息id
    private String url; //点击 跳转的链接，可以为空。
    private Template data; // 模版消息对应的内容数据

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Template getData() {
        return data;
    }

    public void setData(Template data) {
        this.data = data;
    }
}

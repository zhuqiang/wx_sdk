package cn.mrcode.wxsdk.core.dialogue.hander;


import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.TemplateReqMsg;
import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.template.Template;

/**
 * 会话服务，接口请求非空检查处理
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/10 13:28
 */
public class DialogueInterfaceReqDataCheckHander extends InterfaceReqDataCheckHander {
    public static boolean hander(TemplateReqMsg templateReqMsg) {
        String msgPrefix = "发送模版消息api";
        String touser = templateReqMsg.getTouser();
        isBlankErr("touser",touser,msgPrefix,"目标openid不能为空");
        String template_id = templateReqMsg.getTemplate_id();
        isBlankErr("template_id",template_id,msgPrefix,"模版消息id不能为空");
        String url = templateReqMsg.getUrl();

        Template data = templateReqMsg.getData();
        if(data == null){
            throw new IllegalArgumentException(msgPrefix + ":" + "data" + "模版消息内容不能为空");
        }

        return true;
    }
}
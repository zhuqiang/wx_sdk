package cn.mrcode.wxsdk.core.dialogue.common.log;

import cn.mrcode.wxsdk.core.dialogue.common.util.DateUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 日志工具
 *
 * @author zhuqiang
 */
public class LogTrackUtil {
    private static final Log LOGGER = LogFactory.getLog(LogTrackUtil.genFlowNum());
    //存储当次请求追踪数据
    public static final ThreadLocal<LogTrackReqData> TRACK_LOCAL = new ThreadLocal<LogTrackReqData>();

    /**
     * 生成操作流水，用于跟踪一个方法的原子操作
     *
     * @return
     */
    public static String genFlowNum() {
        return RandomStringUtils.random(10, false, true);
    }

    private static String apiName = "[微信sdk日志追踪]";

    /**
     * 追踪开始
     *
     * @param request
     */
    public static void start(HttpServletRequest request) {
        LogTrackReqData logTrackReqData = new LogTrackReqData();
        logTrackReqData.setStartTime(new Date());
        logTrackReqData.setReqId(genFlowNum());
        TRACK_LOCAL.set(logTrackReqData);
        String userAgent = request.getHeader("user-agent");//返回客户端浏览器的版本号、类型
        String method = request.getMethod();//：获得客户端向服务器端传送数据的方法有get、post、put等类型
        String requestURI = request.getRequestURI();//：获得发出请求字符串的客户端地址
        String remoteAddr = request.getRemoteAddr();//：获得客户端的ip地址
//        System.out.println(request.getServletPath()); //：获得客户端所请求的脚本文件的文件路径
//        System.out.println(request.getServerName()); //：获得服务器的名字
//        System.out.println(request.getServerPort()); //：获得服务器的端口号
//        System.out.println(request.getRemoteHost()); //：获得客户端电脑的名字，若失败，则返回客户端电脑的ip地址
        String msg = "请求url=%s;请求方式=%s;浏览器信息=%s,客户端ip=%s,请求开始时间=%s";

        LOGGER.info(LogTemplateUtil.svMsg(apiName, msg,
                requestURI,
                method,
                userAgent,
                remoteAddr,
                DateUtil.llFormat(logTrackReqData.getStartTime())));
    }

    /**
     * 追踪结束
     */
    public static void end() {
        LogTrackReqData logTrackReqData = TRACK_LOCAL.get();
        logTrackReqData.setEndTime(new Date());
        LOGGER.info(LogTemplateUtil.svMsg(apiName,"请求结束时间=%s",DateUtil.llFormat(logTrackReqData.getEndTime())));
        TRACK_LOCAL.remove();
    }
}

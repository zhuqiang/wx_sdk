package cn.mrcode.wxsdk.core.zkhelper.curator;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 *  客户端助手
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/28
 */
public class ClientHelper {
    private ClientHelper(){}

    public static  CuratorFramework createClient(String zkServiceList,int sessionTimeout){
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);

        CuratorFramework client  = CuratorFrameworkFactory.builder().connectString(zkServiceList)
                .sessionTimeoutMs(sessionTimeout)
                .retryPolicy(retryPolicy)
                .namespace("wx")
                .build();
        client.start();
        return createClient(zkServiceList,sessionTimeout,"wx");
    }

    /**
     * 创建客户端
     * @param zkServiceList zk服务器列表
     * @param sessionTimeout 超时 毫秒
     * @param namespace
     * @return
     */
    private static  CuratorFramework createClient(String zkServiceList,int sessionTimeout,String namespace){
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);

        CuratorFramework client  = CuratorFrameworkFactory.builder().connectString(zkServiceList)
                .sessionTimeoutMs(sessionTimeout)
                .retryPolicy(retryPolicy)
                .namespace(namespace)
                .build();
        client.start();

        return client;
    }
}

package cn.mrcode.wxsdk.core.dialogue.service;

import cn.mrcode.wxsdk.core.dialogue.common.SignUtil;
import cn.mrcode.wxsdk.core.dialogue.common.XMLParaser;
import cn.mrcode.wxsdk.core.dialogue.common.exception.ReqException;
import cn.mrcode.wxsdk.core.dialogue.common.exception.WxException;
import cn.mrcode.wxsdk.core.dialogue.common.httpClient.IHttpClinet;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

/**
 * 基础服务超类
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/19 11:08
 */
public class BaseService {
    private static IHttpClinet iHttpClinet;
    private static Logger log = LoggerFactory.getLogger(BaseService.class);

    /**
     * get 请求,请求失败将直接抛出异常reqExecution，<br/>
     * 包括的异常有：请求超时，url地址不合法 等请求错误
     * 统一包装成：code=299000   info=微信远端服务不可用或请求地址错误！
     *
     * @param url url地址
     *
     * @return
     */
    protected static String get(String url) throws ReqException {
        return iHttpClinet.get(url);
    }


    /**
     * post  Json  请求
     *
     * @param url  地址
     * @param body json串
     *
     * @return
     */
    protected static String postJson(String url, String body) throws ReqException {
        return iHttpClinet.postJson(url,body);
    }

    /**
     * xml请求
     * @param url
     * @param xml
     * @param appId 有值则使用ssl证书请求
     *
     * @throws ReqException
     */
    protected static String postXml(String url, String xml,String appId) throws ReqException {
        return iHttpClinet.postXml(url,xml,appId);
    }

    /**
     * 检测微信接口调用是否成功；  只检测 协议  返回结果。是否通过。没有通过则抛出req异常
     *
     * @param resultObj 如果参数为null：则抛出异常信息
     *
     * @throws “sdk.dialogue.common.ReqException” 如果请求结果中，有错误值，则抛出req错误
     */
    protected static void handerThrowErrcode(JSONObject resultObj) throws WxException {
        if (resultObj == null) {
            throw new IllegalArgumentException("参数 resultObj 不能为 null");
        }
        Integer errcode = resultObj.getInteger("errcode");
        String errmsg = resultObj.getString("errmsg");
        if (errcode != null && errcode != 0) {
            log.error("errcode={},errmsg={}",errcode,errmsg);
            throw new WxException(errcode+"",errmsg);
        }
    }

    /**
     * 检测微信接口调用是否成功，业务错误代码则抛出相关异常
     * @param objXml
     * @param key 密钥
     * @return
     */
    public static Map handerPayThrowErrcode(String objXml,String key)throws WxException{
        Map map = null;
        try {
            map = XMLParaser.parseMap(objXml);
        } catch (Exception e) {
            throw new RuntimeException("返回结果xml解析错误");
        }
        String  return_code = (String) map.get("return_code");
        if("FAIL".equals(return_code)) {
            String errMsg = (String) map.get("return_msg");
            if(errMsg != null){
                throw new WxException(return_code,errMsg);
            }
        }else{
            try {
                boolean b = SignUtil.checkIsSignValidFromResponseString(objXml, key);
                if(!b){
                    throw new RuntimeException("API返回的数据签名验证不通过，有可能被第三方篡改!!!");
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }

        return map;
    }
}

package cn.mrcode.wxsdk.core.dialogue.common.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhuqiang
 * @version V1.0
 * @date 2015/11/13 13:59
 */
public final class SdkGlobalCode {

    private static Map<String,String> codeInfoMap = new HashMap<String,String> ();
    /**
     * 获取code对应的信息
     * @param code
     * @return
     */
    public static String getInfo(String code){
        return codeInfoMap.get(code);
    }
    /**
     * 请求器相关的的错误码：
     * req ： 请求器方面的错误异常
     */
    public static String REQ_10000 = "REQ_10000";
    public static String REQ_10001 = "REQ_10001";
    static{
        codeInfoMap.put(REQ_10000,"远端服务不可用，或请求过程出错");
        codeInfoMap.put(REQ_10001,"没有找到可用的请求器");
    }
}

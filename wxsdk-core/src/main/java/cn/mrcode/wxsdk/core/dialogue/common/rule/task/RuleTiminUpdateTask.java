package cn.mrcode.wxsdk.core.dialogue.common.rule.task;

import cn.mrcode.wxsdk.core.dialogue.common.log.LogTemplateUtil;
import cn.mrcode.wxsdk.core.dialogue.common.rule.NewsRule;
import cn.mrcode.wxsdk.core.dialogue.common.rule.Rule;
import cn.mrcode.wxsdk.core.dialogue.common.rule.TextRule;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Description: 规则定时更新，用于定时加载proper中的规则文件。
 *  设计思路： 多个关键字用逗号隔开，然后放入map中。每个关键词引用的都会是同一个实体类，方便查找也不会浪费空间
 *
 *
 *  默认的规则模型暂时只支持：
 *      1. 单关键字 对应 单规则，多关键词 对应单规则，
 *      2. 消息类型规则支持：文本消息、单图文消息
 *   一般在项目中都会把关键词放入数据库中等操作，或则缓存中操作，这里只是提供一种思路
 *  使用方式：
 *      1. 项目启动后，启动本task，让rules中有关键词源
 *      2. 在消息事件中，从rules中取出rule，然后根据type判定其类型（强制转型为具体的子类，来获取更详细的信息），然后拼接对应的消息类型返回（返回的时候使用协议中的outMsg数据包哦）
 * <br>
 *
 * @author zhuqiang
 */
public class RuleTiminUpdateTask implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(RuleTiminUpdateTask.class);
    public static HashMap<String, Rule> rules = new HashMap<>();
    private Properties pr = new Properties();
    private String apiName = "规则定时更新";

    @Override
    public void run() {
        try {
            while (true) {
                load();
                TimeUnit.SECONDS.sleep(60*3);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void load() {
        try {
            pr.load(getClass().getResourceAsStream("/wxrule.properties"));
            Enumeration<?> enumeration = pr.propertyNames();
            rules.clear();
            while (enumeration.hasMoreElements()) {
                String ruleName = (String) enumeration.nextElement();
                String ruleJson = pr.getProperty(ruleName);
                Rule rule = JSONObject.parseObject(ruleJson, Rule.class);
                String type = rule.getType();
                switch (type) {
                    case Rule.TYPE_TEXT:
                        rule = JSONObject.parseObject(ruleJson,TextRule.class);
                        break;
                    case Rule.TYPE_NEWS:
                        rule = JSONObject.parseObject(ruleJson, NewsRule.class);
                        break;
                }
                String keyword = rule.getKeyword();
                String[] keywords = keyword.split(",");
                for (String key : keywords) {
                    rules.put(key, rule);
                }
            }
        } catch (IOException e) {
            log.error(LogTemplateUtil.svMsg(apiName, "未找到规则文件"));
        }
    }
}

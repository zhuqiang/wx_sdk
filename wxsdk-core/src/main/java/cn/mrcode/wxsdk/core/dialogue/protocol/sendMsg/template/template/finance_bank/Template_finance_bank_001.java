package cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.template.finance_bank;

import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.template.Template;
import com.alibaba.fastjson.annotation.JSONField;


/**
 * 交易成功通知	<pre>金融业	银行 id为：dEBDxLv4IcPv86-xP_KUoyoPpFeAj95RE-wMkD2C0TA
    模版：
     {{first.DATA}}

     {{type.DATA}}金额：{{num.DATA}}
     {{accountType.DATA}}：{{account.DATA}}
     交易时间：{{time.DATA}}
     交易单号：{{order.DATA}}
     {{remark.DATA}}
     示例：
     理财通-转出到银行卡

     转出金额：1000元
     银行账户： 招商银行（卡尾号1234）
     交易时间：2014-01-10 10：50
     交易单号：xxx</pre>
 * @author zhuqiang
 * @version V1.0
 * @Description:具体的模版消息内容实现
 * @date 2015/10/10 11:29
 */
public class Template_finance_bank_001 extends Template {
    @JSONField(serialize=false)
    public static String ID = "dEBDxLv4IcPv86-xP_KUoyoPpFeAj95RE-wMkD2C0TA";
    private Data first;
    private Data type;
    private Data num;
    private Data time;
    private Data order;
    private Data accountType;
    private Data account;

    public static String getID() {
        return ID;
    }

    public static void setID(String ID) {
        Template_finance_bank_001.ID = ID;
    }

    public Data getFirst() {
        return first;
    }

    public void setFirst(Data first) {
        this.first = first;
    }

    public Data getType() {
        return type;
    }

    public void setType(Data type) {
        this.type = type;
    }

    public Data getNum() {
        return num;
    }

    public void setNum(Data num) {
        this.num = num;
    }

    public Data getTime() {
        return time;
    }

    public void setTime(Data time) {
        this.time = time;
    }

    public Data getOrder() {
        return order;
    }

    public void setOrder(Data order) {
        this.order = order;
    }

    public Data getAccountType() {
        return accountType;
    }

    public void setAccountType(Data accountType) {
        this.accountType = accountType;
    }

    public Data getAccount() {
        return account;
    }

    public void setAccount(Data account) {
        this.account = account;
    }
}
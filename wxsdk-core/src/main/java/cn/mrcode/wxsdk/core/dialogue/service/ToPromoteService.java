package cn.mrcode.wxsdk.core.dialogue.service;

import cn.mrcode.wxsdk.core.dialogue.common.exception.ReqException;
import cn.mrcode.wxsdk.core.dialogue.common.exception.WxException;
import cn.mrcode.wxsdk.core.dialogue.protocol.toPromote.req.QrCodeReqData;
import cn.mrcode.wxsdk.core.dialogue.protocol.toPromote.res.QrCodeResData;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 推广服务：二维码服务 与  长链接转短链接
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/21 13:52
 */
public class ToPromoteService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(ToPromoteService.class);
    /** 根据ticket直接获取二维码地址*/
    public static final String SHOWQRCODE_API = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";
    /** 二维码生成地址 */
    public static final String CREATE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=";

    /**
     * 创建二维码
     * @param qrCodeReqData
     * @param accessToken
     * @return 返回二维码信息对象对象
     * @throws ReqException 抛出请求异常，和 访问该api的业务异常
     */
    public static QrCodeResData createQrCode(QrCodeReqData qrCodeReqData, String accessToken) throws ReqException, WxException {
        String result = postJson(CREATE_URL + accessToken, JSON.toJSONString(qrCodeReqData));
        JSONObject resultObj = JSONObject.parseObject(result);
        handerThrowErrcode(resultObj);
        QrCodeResData qrCodeResData = JSONObject.parseObject(result, QrCodeResData.class);
        return qrCodeResData;
    }
}

package cn.mrcode.wxsdk.core.dialogue.protocol.msg.out;


import cn.mrcode.wxsdk.core.dialogue.common.MsgType;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.OutMsg;

/**
 *   多客服转接
     <xml>
        <ToUserName><![CDATA[touser]]></ToUserName>
        <FromUserName><![CDATA[fromuser]]></FromUserName>
        <CreateTime>1399197672</CreateTime>
        <MsgType><![CDATA[transfer_customer_service]]></MsgType>
     </xml>
 * @author zhuqiang
 */
public class TransferCustomerServiceOutMsg  extends OutMsg {
    public static final String TEMPLATE = "<xml>\n"
            + "<ToUserName><![CDATA[${__msg.toUserName}]]></ToUserName>\n"
            + "<FromUserName><![CDATA[${__msg.fromUserName}]]></FromUserName>\n"
            + "<CreateTime>${__msg.createTime}</CreateTime>\n"
            + "<MsgType><![CDATA[${__msg.msgType}]]></MsgType>\n</xml>";
    public TransferCustomerServiceOutMsg(){}
    public TransferCustomerServiceOutMsg(InMsg inMsg) {
        super(inMsg);
        this.msgType = MsgType.RES_TRANSFER_CUSTOMER_SERVICE.value();
    }

}

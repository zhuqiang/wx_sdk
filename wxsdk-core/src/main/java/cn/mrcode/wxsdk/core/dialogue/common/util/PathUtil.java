package cn.mrcode.wxsdk.core.dialogue.common.util;

import java.net.URL;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 路径相关
 * @date 2015/8/28 16:13
 */
public class PathUtil {
    /**
     * 获取webroot根目录路径
     * @return
     */
    public static String getWrbRootPath() {
        URL uri = PathUtil.class.getResource("/");
        //str会得到这个函数所在类的路径
        String str = uri.toString();
        //截去一些前面6个无用的字符
        str = str.substring(6, str.length());
        //将%20换成空格（如果文件夹的名称带有空格的话，会在取得的字符串上变成%20）
        str = str.replaceAll("%20", " ");
        //查找“WEB-INF”在该字符串的位置
        int num = str.indexOf("WEB-INF");
        //截取即可
        str = str.substring(0, num);
        return str;
    }
}

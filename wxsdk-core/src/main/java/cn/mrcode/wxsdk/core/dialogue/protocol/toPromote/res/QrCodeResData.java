package cn.mrcode.wxsdk.core.dialogue.protocol.toPromote.res;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 二维码回包数据，注意：取ticket的时候，如果需要拿ticket去换取二维码在线地址，请调用处理后的ticket
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/21 14:02
 */
public class QrCodeResData {
    private String ticket; //  获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
    private int expire_seconds; //  二维码的有效时间，以秒为单位。最大不超过1800。
    private String url; //  二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片

    /**
     * 该方法用于把 ticket urlEncode，转码后返回，用于换取二维码图片
     * @return
     */
    public String urlEncodeTicket(){
        String result = null;
        try {
            result = URLEncoder.encode(ticket, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return  result;
    }
    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getExpire_seconds() {
        return expire_seconds;
    }

    public void setExpire_seconds(int expire_seconds) {
        this.expire_seconds = expire_seconds;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

package cn.mrcode.wxsdk.core.dialogue.protocol.toPromote.req;

/**
 * 二维码请求数据
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/21 14:02
 */
public class QrCodeReqData {
    /** 临时二维码*/
    public static final String QR_SCENE = "QR_SCENE";
    /** 永久二维码*/
    public static final String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";

    private Integer expire_seconds; //该二维码有效时间，以秒为单位。 最大不超过604800（即7天）。
    private String action_name; //二维码类型，QR_SCENE为临时,QR_LIMIT_SCENE为永久,QR_LIMIT_STR_SCENE为永久的字符串参数值

    /**
     * 该场景值：在永久二维码中， 只能是数字，而在 临时二维码中发现，字符串也可以
     */
    private transient  Integer scene_id; //场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
    private transient  String scene_str; //场景值ID（字符串形式的ID），字符串类型，长度限制为1到64，仅永久二维码支持此字段
    private Action_info action_info = new Action_info(); //二维码详细信息

    public Integer getExpire_seconds() {
        return expire_seconds;
    }

    class Action_info{
        private Scene scene = new Scene();

        public Scene getScene() {
            return scene;
        }

        public void setScene(Scene scene) {
            this.scene = scene;
        }

        class Scene{
            private  Integer scene_id; //场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
            private  String scene_str; //场景值ID（字符串形式的ID），字符串类型，长度限制为1到64，仅永久二维码支持此字段

            public Integer getScene_id() {
                return scene_id;
            }

            public void setScene_id(Integer scene_id) {
                this.scene_id = scene_id;
            }

            public String getScene_str() {
                return scene_str;
            }

            public void setScene_str(String scene_str) {
                this.scene_str = scene_str;
            }
        }
    }

    /**
     * 该二维码有效时间，以秒为单位。 最大不超过604800（即7天）。
     * @param expire_seconds
     */
    public void setExpire_seconds(Integer expire_seconds) {
        this.expire_seconds = expire_seconds;
    }

    public String getAction_name() {
        return action_name;
    }

    /**
     * 二维码类型，QR_SCENE为临时,QR_LIMIT_SCENE为永久,QR_LIMIT_STR_SCENE为永久的字符串参数值<br/>
     * 二维码类型 ，请选择本类常量,永久二维码，将置空 二维码有效时间
     * @param action_name
     */
    public void setAction_name(String action_name) {
        setExpire_seconds(null);
        this.action_name = action_name;
    }

    public Integer getScene_id() {
        return scene_id;
    }

    /**
     * 场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
     * 该场景值是二维码的唯一值，换句话说，该值决定了你获取的是哪一个二维码图片
     * @param scene_id
     */
    public void setScene_id(Integer scene_id) {
        action_info.scene.scene_id = scene_id;
        this.scene_id = scene_id;
    }

    public String getScene_str() {
        return scene_str;
    }

    /**
     * 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64，仅永久二维码支持此字段
     * @param scene_str
     */
    public void setScene_str(String scene_str) {
        action_info.scene.scene_str = scene_str;
        this.scene_str = scene_str;
    }

    public Action_info getAction_info() {
        return action_info;
    }
}

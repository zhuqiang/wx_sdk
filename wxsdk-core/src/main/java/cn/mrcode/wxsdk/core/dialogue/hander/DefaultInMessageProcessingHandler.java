package cn.mrcode.wxsdk.core.dialogue.hander;


import cn.mrcode.wxsdk.core.dialogue.common.OutMsgXmlBuilder;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.in.*;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.in.event.*;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.out.TextOutMsg;

/**
 * Created by zhuqiang on 2015/8/18.
 * 默认请求消息处理
 */
//@Component("defaultInMessageProcessingHandler")   // 用于spring环境
//@Lazy(false)
public final class DefaultInMessageProcessingHandler extends InMessageProcessingHandler<String> {
    @Override
    public String processTextMsg(TextInMsg textInMsg, String appId, String paramObj) {
        // 可根据文本内容查找对应的回复消息，或则根据自己的业务规则 创建对应的回复消息
        return null;
    }

    @Override
    public String processImageMsg(ImageInMsg imageInMsg, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processVoiceMsg(VoiceInMsg voiceInMsg, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processVideoMsg(VideoInMsg videoInMsg, String appId, String paramObj) {
        return null;
    }

    @Override
    protected String processShortVideoMsg(ShortVideoInMsg shortVideoInMsg, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processLocationMsg(LocationInMsg locationInMsg, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processLinkMsg(LinkInMsg linkInMsg, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processFollowEvent(FollowInEvent followInEvent, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processSubscribeEvent(FollowInEvent followInEvent, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processQrCodeEvent(QrCodeInEvent qrCodeInEvent, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processLocationEvent(LocationInEvent locationInEvent, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processMenuEvent(MenuInEvent menuInEvent, String appId, String paramObj) {
        return null;
    }

    @Override
    protected String processMenuLocationSelectEvent(MenuLocationSelectInEvent menuLocationSelectInEvent, String appId, String paramObj) {
        return null;
    }

    @Override
    protected String processTemplateSendJobFinishInEvent(TemplateSendJobFinishInEvent msg, String appId, String paramObj) {
        return null;
    }

    @Override
    public String processSpeechRecognitionResults(SpeechRecognitionResults speechRecognitionResults, String appId, String paramObj) {
        return null;
    }

    @Override
    public String defaultProcess(InMsg inMsg, String appId, String paramObj) {
        TextOutMsg outTextMsg = new TextOutMsg(inMsg);
        outTextMsg.setContent("该消息服务器未能处理；【消息类型：" + inMsg.getMsgType() + "，appId=" + appId + "】");
        String result = OutMsgXmlBuilder.build(outTextMsg);
        return result;
    }

}

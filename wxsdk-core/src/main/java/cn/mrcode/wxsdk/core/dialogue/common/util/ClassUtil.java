package cn.mrcode.wxsdk.core.dialogue.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/20 16:17
 */
public class ClassUtil {
    private static Logger log = LoggerFactory.getLogger(DateUtil.class);
    /**
     * 设置 静态属性的值
     * @param cls 目标class
     * @param filedName 字段名称
     * @param value 改变的值
     * @return
     */
    public static boolean setStaticField(Class<?> cls,String filedName,Object value){
        Field field = null;
        try {
            field = cls.getDeclaredField(filedName);
            field.setAccessible(true);
            field.set(field, value);
        } catch (Exception e) {
            log.error("can not set static field by : " + e);
            throw new RuntimeException(e);
        }
        return true;
    }

    public static Object newInstance(String className){
        try {
            Class c = Class.forName(className);
            return c.newInstance();
        } catch (Exception e) {
            log.error(String.format("newInstance failure : className=%s;",className) + e);
            throw new RuntimeException(e);
        }
    }
}

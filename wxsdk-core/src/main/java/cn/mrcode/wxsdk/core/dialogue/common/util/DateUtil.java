package cn.mrcode.wxsdk.core.dialogue.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with easyExchangeShop.
 *
 * @author zhuqiang
 * @version V1.0
 * @Description 时间工具类
 * @date 2015/8/19 15:22
 */
public class DateUtil {
    private DateUtil(){}
    private static Logger log = LoggerFactory.getLogger(DateUtil.class);
    /**
     * @Title: computingTime
     * @Description: 在一个日期对象上 计算时间，加减对应的日期
     * @param startDate	当前时间
     * @param field  calendar 中的时分秒等常量
     * @param amount 对应常量的时间值（正数则加，负数减少）
     * @return Date 处理后的日期对象
     */
    public static Date computingTime(Date startDate,int field,int amount){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        log.info(calendar.getTime() + "");
        calendar.add(field, amount);
        log.info(calendar.getTime() + "");
        Date date = calendar.getTime();
        return date;
    }

    /**
     * 获取单位为秒的时间戳
     * @return
     */
    public static String getNowTimestamp(){
        long result = System.currentTimeMillis() / 1000;
        return result + "";
    }

    /**
     * 长时间格式化 yyyy-MM-dd HH:mm:ss
     * @param date
     * @return
     */
    public static String lFormat(Date date) {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String t = "";
        try {
            t = s.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }
    /**
     * 长时间格式化 yyyy-MM-dd HH:mm:ss
     * @param date
     * @return
     */
    public static String llFormat(Date date) {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String t = "";
        try {
            t = s.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }
}

package cn.mrcode.wxsdk.core.dialogue.common.exception;

/**
 * @author zhuqiang
 * @version V1.0
 * @date 2015/11/13 13:09
 * SDK异常信息，会定制一些sdk规则的异常。某一些些异常是由原生的异常对象包装而来。所以也会提供原生的异常对象
 */
public class SdkException extends RuntimeException {
    private String code; //异常编码
    private String info;  //异常消息
    private Exception nativeException; //原生异常对象

    public SdkException(String code, String info){
        this.code = code;
        this.info = info;
    }

    public SdkException(String code, String info, Exception nativeException){
        this.nativeException = nativeException;
        this.code = code;
        this.info = info;
    }

    /** 该方法始终会返回错误消息，是原生异常消息，则返回原生异常消息，否则返回info */
    @Override
    public String getMessage() {
        return isNativeException() ? this.info:nativeException.getMessage();
    }

    /** 该异常是否是 原生的异常信息， */
    public boolean isNativeException(){
        return nativeException == null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}

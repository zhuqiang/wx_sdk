package cn.mrcode.wxsdk.core.dialogue.protocol.msg.in.event;


import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;

/**
 * 自定义菜单事件 1： 点击菜单拉取消息时的事件推送 <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[CLICK]]></Event> <EventKey><![CDATA[EVENTKEY]]></EventKey>
 * </xml>
 * 
 * 2： 点击菜单跳转链接时的事件推送 <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[VIEW]]></Event> <EventKey><![CDATA[www.qq.com]]></EventKey>
 * </xml>
 */
public class MenuInEvent extends InMsg {
	private String event;
	private String eventKey;

	public MenuInEvent(String toUserName, String fromUserName,
					   Integer createTime, String msgType) {
		super(toUserName, fromUserName, createTime, msgType);
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
}

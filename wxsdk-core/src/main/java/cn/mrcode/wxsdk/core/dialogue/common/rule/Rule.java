package cn.mrcode.wxsdk.core.dialogue.common.rule;

/**
 规则实体
 * @author zuqiang
 */
public class Rule  {
    /** 文字消息 */
    public static final String TYPE_TEXT = "text";
    /** 图文消息 */
    public static final String TYPE_NEWS = "news";

    //关键词
    protected String keyword;
    protected String type; //消息类型

    public Rule() {
    }

    public Rule(String keyword, String type) {
        this.keyword = keyword;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}

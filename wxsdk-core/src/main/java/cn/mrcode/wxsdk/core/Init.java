package cn.mrcode.wxsdk.core;

import cn.mrcode.wxsdk.core.context.ConfigContext;

/**
 * sdk 初始化接口
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/15 0015 17:02
 */
public interface Init {
    /**
     * 初始化
     * @param configContext 配置参数上下文
     */
    void init(ConfigContext configContext);

    /**
     * 获取配置属性上下文
     * @return
     */
    ConfigContext getConfigContext();
}

package cn.mrcode.wxsdk.core.zkhelper.curator;

import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  路径助手
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/28
 */
public class ZkPathHelper {
    private static Logger log = LoggerFactory.getLogger(ZkPathHelper.class);
    private ZkPathHelper(){}

    /**
     * 如果指定的路径已经存在，则不创建
     * 不存在则创建
     * @param client 包含了namespace的客户端链接
     * @param path 要创建的路径
     * @param initData 初始化数据
     * @return
     */
    public static boolean createIfExistsNot(CuratorFramework client,String path,byte[] initData){
        //创建维护节点
        try {
            if(client.checkExists().forPath(path) == null){
                client.create().forPath(path,"".getBytes());
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("创建失败：" + e.getMessage());
            return false;
        }
        return true;
    }
}

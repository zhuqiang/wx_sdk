package cn.mrcode.wxsdk.core.context;

/**
 * sdk 上下文
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/15 16:59
 */
public class SdkContexts {
    //所有的配置参数
    private static ConfigContext configContext;

    public static ConfigContext getConfigContext() {
        return configContext;
    }

    public static void setConfigContext(ConfigContext configContext) {
        SdkContexts.configContext = configContext;
    }
}

package cn.mrcode.wxsdk.core.dialogue.common.exception;

/**
 * Created by zhuqiang on 2015/8/19.
 * http请求器请求过程中的异常信息<br/>
 * code ：异常编码<br/>
 * info: 异常消息<br/>
 * nativeException:原生的 异常对象：http请求器发出的异常信息 将有值<br/>
 */
public class ReqException extends SdkException {
    public ReqException(String sdkGlobalCode) {
        super(sdkGlobalCode, SdkGlobalCode.getInfo(sdkGlobalCode));
    }
    public ReqException(String sdkGlobalCode, Exception nativeException) {
        super(sdkGlobalCode, SdkGlobalCode.getInfo(sdkGlobalCode), nativeException);
    }
}

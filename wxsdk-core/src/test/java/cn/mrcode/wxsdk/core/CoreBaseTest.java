package cn.mrcode.wxsdk.core;

import cn.mrcode.wxsdk.core.test.BaseTest;

/**
 * core 测试基类
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/17 0017 9:50
 */
public class CoreBaseTest extends BaseTest {
    public CoreBaseTest() {
        super(CoreInit.class);
    }
}

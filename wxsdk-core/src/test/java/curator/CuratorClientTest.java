package curator;

import com.alibaba.fastjson.JSON;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListenerAdapter;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * Description: zk apach 客户端<br>
 * Date: 2016年04月20日 <br>
 *
 * @author zhuqiang
 */
public class CuratorClientTest {
    /** Zookeeper info */
    private static final String ZK_ADDRESS = "192.168.5.75:2181";
    private static final String ZK_PATH = "/zktest";

    public static void main(String[] args) throws Exception {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000,3);
        final CuratorFramework client =
        CuratorFrameworkFactory.builder().connectString(ZK_ADDRESS)
                .sessionTimeoutMs(5000)
                .retryPolicy(retryPolicy)
                .namespace("wx")
                .build();
        client.start();
        System.out.println("链接成功");
        final String masterPath = "/basetoken";
        LeaderSelector selector = new LeaderSelector(client, masterPath, new LeaderSelectorListenerAdapter() {
            @Override
            public void takeLeadership(CuratorFramework curatorFramework) throws Exception {
                System.out.println("成为masetr角色");
                String firstBody = new String(curatorFramework.getData().forPath(masterPath));
                System.out.println("首次获得数据：" + firstBody);
                int i = 0;
                while (true){
                    Thread.sleep(3000);
                    String s = new String(client.getData().forPath(masterPath));
                    System.out.println(s + "," + curatorFramework.getState());
                    client.setData().forPath(masterPath,("xxxxx" + i++).getBytes());
                    System.out.println("完成masetrcap操作" + JSON.toJSONString(curatorFramework));
                }
            }
        });
        selector.autoRequeue();
        selector.start();
        final NodeCache nodeCache = new NodeCache(client, masterPath, false);
        nodeCache.start(true);
        nodeCache.getListenable().addListener(new NodeCacheListener() {
            @Override
            public void nodeChanged() throws Exception {
                String s = new String(nodeCache.getCurrentData().getData());
                System.out.println("节点更新了：" + s);
            }
        });

        while (true){
            Thread.sleep(3000);
            System.out.println("sssss");
        }
//        Thread.sleep(Integer.MAX_VALUE);

//        client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath(ZK_PATH,"2xxxxxx".getBytes());

    }

}

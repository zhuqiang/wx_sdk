package curator;

import cn.mrcode.wxsdk.core.CoreBaseTest;
import cn.mrcode.wxsdk.core.context.ConfigContext;
import cn.mrcode.wxsdk.core.zkhelper.curator.ClientHelper;
import cn.mrcode.wxsdk.core.zkhelper.curator.CoreResetDataHelper;
import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 手动重置 zk上的数据
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/28
 */
public class CoreResetZkData extends CoreBaseTest {
    private static Logger log = LoggerFactory.getLogger(CoreResetZkData.class);
    private static int sessionTimeout;
    private static String zkServiceList;

    public CoreResetZkData() {
        ConfigContext.DistributedConfig dirstributedConfig = configContext.getDirstributedConfig();

        sessionTimeout = dirstributedConfig.getSessionTimeout();
        zkServiceList = dirstributedConfig.getZkServiceList();
    }

    public void reset() throws Exception {
        CuratorFramework client = ClientHelper.createClient(zkServiceList, sessionTimeout);
        CoreResetDataHelper.resetToken(client,configContext);
        client.close();
    }
    public static void main(String[] args) throws Exception {
       new CoreResetZkData().reset();
        System.out.println("更新成功！");
        System.exit(0);
    }
}
